import React from 'react';
import axios from 'axios';
import {connect} from 'react-redux';
import {DeleteModal,DetailsModal,Menubar} from './index';
import {ReactSearchAutocomplete} from 'react-search-autocomplete';
import {Sidebar,Menu,Icon,Accordion} from 'semantic-ui-react';


class ProductManage extends React.Component {
    state = { 
    productList: [],
    searchProductList:[],
    id:"",
    name:"",
    description:"",
    price:0,
    amount:0,
    insertButton:"Add New",
    editData:false,
    activeIndex:0
    }
    componentDidMount(){
        this.getData();
       
    } 
    getData= async()=>{
        const response = await axios.get('http://localhost:8080/api/products');
        this.setState({productList:response.data});
        this.setState({searchProductList:response.data});
    }
    getDataByName = async (name)=> {
        const response = await axios.get("http://localhost:8080/api/products/name/"+name);
        console.log("---");
        console.log(response);
        console.log("---");
        const data = []
        data.push(response.data)
        this.setState({productList:data});
    }

    async save(){
        if(this.state.id){
            await axios.post("http://localhost:8080/api/products",{
            id:this.state.id,
            name:this.state.name,
            description:this.state.description,
            price:this.state.price,
            amount:this.state.amount
        });
        }else{
            await axios.post("http://localhost:8080/api/products",{
                name:this.state.name,
                description:this.state.description,
                price:this.state.price,
                amount:this.state.amount
            });
        }
        this.resetForm();
 
    }
    resetForm(){
        this.setState({id:""})
        this.setState({name:""})
        this.setState({description:""})
        this.setState({price:0})
        this.setState({amount:0})
        this.setState({insertButton:"Add New"})
        this.setState({editData:false})
        this.getData();
    }
    renderButtonInsertStyle(){
        if(this.state.insertButton==="Save Changes"){
            return "ui orange button"
        }
        return "ui teal button"
    }
    selectData(data,action){
        if(action==="Save Changes"){
            this.setState({id:data.id})
            this.setState({name:data.name})
            this.setState({description:data.description})
            this.setState({price:data.price})
            this.setState({amount:data.amount})
            this.setState({insertButton:"Save Changes"})
            this.setState({editData:true})
        }else{
            this.setState({id:data.id})
            this.setState({name:data.name})
            this.setState({description:data.description})
            this.setState({amount:data.amount})
            this.getData();
        }

    }
    

    renderButtonChange(){
        if(this.state.insertButton==="Save Changes"){
            return <button className="ui red button" onClick={()=>this.resetForm()}>Cancel</button>
        }
    }
    renderProductList(){
        return this.state.productList.map((data,key)=>
            <tr key={key}>
                <td data-label="Name">{data.name}</td>
                <td data-label="Description">{data.description}</td>
                <td data-label="Description">{data.price}</td>
                <td data-label="Stock">{data.amount}</td>
                <td>
                    <button className="ui orange button" 
                    onClick={()=>{
                        this.selectData(data,"Save Changes")
                    }}>
                        Change
                    </button>
                    <DeleteModal deleteData={
                        {
                            id:data.id,
                            name:data.name,
                            description:data.description,
                            amount:data.amount
                        } 
                    } reFetch={this.getData} onClick={()=>this.selectData(data,"Delete")}/>
                    <DetailsModal deleteData={
                        {
                            id:data.id,
                            name:data.name,
                            description:data.description,
                            amount:data.amount
                        } 
                    } reFetch={this.getData} onClick={()=>this.selectData(data,"Details")}/>
                </td>
            </tr>
        )
    }
    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
      }

    handleOnSearch = (v) => {
        if(v===""||v===" "){
            this.getData();
        }
      }
    
      handleOnHover = (result) => {
      }
    
      handleOnSelect = (item) => {
        this.getDataByName(item.name);
      }
    
      handleOnFocus = () => {
      }
    
    render() {
        const { activeIndex } = this.state
        return (
    <div>
        <Menubar/>
            <Sidebar
            as={Menu}
            animation='overlay'
            icon='labeled'
            vertical
            visible
            width='thin'
            >
            <div ></div>
            <Accordion styled>
                <Accordion.Title
                active={activeIndex === 0}
                index={0}
                onClick={this.handleClick}
                style={{textAlign:"left"}}
                >
                <Icon name='dropdown' />
                financials
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                <p>
                Test
                </p>
                </Accordion.Content>

                <Accordion.Title
                active={activeIndex === 1}
                index={1}
                onClick={this.handleClick}
                style={{textAlign:"left"}}
                >
                <Icon name='dropdown' />
                Sales
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 1}>
                <p>
                    Test
                </p>
                </Accordion.Content>
                <Accordion.Title
                active={activeIndex === 2}
                index={2}
                onClick={this.handleClick}
                style={{textAlign:"left"}}
                >
                <Icon name='dropdown' />
                Data Sources
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 2}>
                <p>
                Test
                </p>
                </Accordion.Content>
                <Accordion.Title
                active={activeIndex === 3}
                index={3}
                onClick={this.handleClick}
                style={{textAlign:"left"}}
                >
                <Icon name='dropdown' />
                Other
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 3}>
                <p>
                Test
                </p>
                </Accordion.Content>
            </Accordion>
            </Sidebar>
            <div style={{width:"300px",height:"70px",marginLeft:"30%"}}>
            <ReactSearchAutocomplete
            items={this.state.searchProductList}
            onSearch={(v)=>this.handleOnSearch(v)}
            onHover={()=>this.handleOnHover()}
            onSelect={(v)=>this.handleOnSelect(v)}
            onFocus={()=>this.handleOnFocus()}
            placeholder="Search Products"
            autoFocus
          />
            </div>
            <div style={{marginLeft:"15%"}}>
            <div style={{width:"90%",display:"flex",alignSelf:"center"}}>
            <table className="ui celled table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Desc</th>
                    <th>Price</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    {this.renderProductList()}
                </tbody>
            </table>
            </div>
            <div style={{height:"50px"}}/>
            <div>
    <div className="ui form">
        <div className="fields">
        <div className="field">
            <label>Product Name</label>
            <input type="text" name="name" placeholder="Product Name" 
            onChange={(v)=>this.setState({name:v.target.value})}
            value={this.state.name} disabled={this.state.editData}
           />
        </div>
        <div className="field">
            <label>Product Description</label>
            <input type="text" name="description" placeholder="Product Description" 
            onChange={(v)=>this.setState({description:v.target.value})}
            value={this.state.description}
            />
        </div>
        <div className="field">
            <label>Price</label>
            <input type="number" name="price" placeholder="Price" 
            onChange={(v)=>this.setState({price:v.target.value})}
            value={this.state.price}
            />
        </div>
        <div className="field">
            <label>Amount</label>
            <input type="number" name="amount" placeholder="Amount" 
           onChange={(v)=>this.setState({amount:v.target.value})}
           value={this.state.amount}/>
        </div>
        </div>
    </div>
    <button 
    className={this.renderButtonInsertStyle()}
    onClick={()=>this.save(
        this.state.name,
        this.state.description,
        this.state.price,
        this.state.amount)}>
        {this.state.insertButton}
    </button>
    {this.renderButtonChange()}
    </div> 
    </div>
        </div> 
        );
    }
}


const mapStateToProps = (state) => ({
    count: state.count,
    reload: state.reload
});
 
export default connect(mapStateToProps)(ProductManage);