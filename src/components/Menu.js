import React from 'react';
import { Dropdown,Image } from 'semantic-ui-react'

function Menu(){
    const style={
        boxShadow: "1px 1px 10px #9E9E9E",
        marginTop:"-1%",
        marginBottom:"30px",
        height:"45px",
        display:"flex",
        justifyContent:"flex-end",
        
    }

return(
    <div style={style}>
                <Dropdown text='New'style={{marginRight:"20px"}}>
                    <Dropdown.Menu>
                    <Dropdown.Item text='Open...' description='ctrl + o' />
                    <Dropdown.Item text='Save as...' description='ctrl + s' />
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown text='Content'style={{marginRight:"20px"}}>
                    <Dropdown.Menu>
                    <Dropdown.Item text='New' />
                    <Dropdown.Item text='Open...' description='ctrl + o' />
                    <Dropdown.Item text='Save as...' description='ctrl + s' />
                    </Dropdown.Menu>
                </Dropdown>
                <Dropdown text='Admin' style={{marginRight:"20px"}}>
                    <Dropdown.Menu>
                    <Dropdown.Item text='Settings' />
                    </Dropdown.Menu>
                </Dropdown>
                <div style={{marginRight:"1%",display:"flex",justifyItems:"center"}} >
                <img width="30px" height="30px"/>
                <Dropdown text='Profile' style={{marginLeft:"3px"}}>
                    <Dropdown.Menu>
                    <Dropdown.Item text='Logout' />
                    </Dropdown.Menu>
                </Dropdown>
                </div>
            </div>

)}

export default Menu

