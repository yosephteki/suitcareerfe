import axios from "axios";
import { useState } from "react";
import { connect } from 'react-redux';

function ProductForm(props) {

    const save = async (inputName,inputDesc,inputAmount)=>{
        await axios.post("http://localhost:8080/api/products",{
            name:inputName,
            description:inputDesc,
            amount:inputAmount
        });
        setName("")
        setDesc("")
        setAmount("")
        // window.location.reload();
        // const action = {type:"RELOAD",payload:true}
        // props.dispatch(action);
    };
    // console.log(props);
    const[name,setName] = useState("")
    const[description,setDesc] = useState("")
    const[amount,setAmount] = useState(0)
    
    // if(props.selectedProduct){
    //     setName(props.selectedProduct.name);
    // }
    
    
    return(
    <div>
    <div className="ui form">
        <div className="fields">
        <div className="field">
            <label>Product Name</label>
            <input type="text" name="name" placeholder="Product Name" 
            onChange={(v)=>setName(v.target.value)}
            value={name}
        //    value={props.selectedProduct?props.selectedProduct.name:name}
           />
        </div>
        <div className="field">
            <label>Product Description</label>
            <input type="text" name="description" placeholder="Product Description" 
            onChange={(v)=>setDesc(v.target.value)}
            value={description} 
            />
        </div>
        <div className="field">
            <label>Amount</label>
            <input type="number" name="amount" placeholder="Amount" 
            onChange={(v)=>setAmount(v.target.value)} 
            value={amount}
            />
        </div>
        </div>
    </div>
    <button className="ui teal button" onClick={()=>save(name,description,amount)}>Save</button>
    </div>
)
}

const mapStateToProps = state => ({
    count : state.count,
    selectedProduct : state.selectedProduct
})

export default connect(mapStateToProps)(ProductForm);




