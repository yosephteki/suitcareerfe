import React from 'react';
import { Button, Header, Modal,Image } from 'semantic-ui-react';


function DetailsModal(props) {
  const [open, setOpen] = React.useState(false)
//   const reload = () => {
//       props.reFetch();
//   }
  
  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button className="ui blue button">Details</Button>}
    >
      <Modal.Header>Delete Product</Modal.Header>
      <Modal.Content image>
       <Image size='medium' src={window.location.origin + '/undraw_logistics_x4dc.svg'} wrapped />
        <Modal.Description>
          <Header>{props.deleteData.name}</Header>
          <p>
           {props.deleteData.description}
          </p>
          <p style={{fontWeight:"1000"}}>Amount : {props.deleteData.amount}</p>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => setOpen(false)}>
          Close
        </Button>
      </Modal.Actions>
    </Modal>
  )
}

export default DetailsModal
