import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
// import reportWebVitals from './reportWebVitals';
const initialState = {
  count:0,
  selectedProduct:{
    id:"",
    name:"",
    description:"",
    amount:0
  },
  reload:false
};

function reducer(state=initialState,action){
  // console.log(action.type)
  switch(action.type){
    case "INCREMENT":
      return{
        count: state.count+1
      }
    case "DECREMENT":
      return{
        count : state.count-1
      }
    case "RELOAD":
      return Object.assign({},state.reload,{reload:action.payload})
    case "EDIT":
      return Object.assign({},state.selectedProduct,{selectedProduct:action.payload});
    default:
      return state;
  }
}

const store = createStore(reducer);
store.dispatch({type:"INCREMENT"});

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
